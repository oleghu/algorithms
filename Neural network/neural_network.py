# Use Python 3.8 or newer (https://www.python.org/downloads/)
import unittest
# Remember to install numpy (https://numpy.org/install/)!
import numpy as np
import pickle
import os


class NeuralNetwork:
    """Implement/make changes to places in the code that contains #TODO."""

    def __init__(self, input_dim: int, hidden_layer: bool) -> None:
        """
        Initialize the feed-forward neural network with the given arguments.
        :param input_dim: Number of features in the dataset.
        :param hidden_layer: Whether or not to include a hidden layer.
        :return: None.
        """

        # --- PLEASE READ --
        # Use the parameters below to train your feed-forward neural network.

        # Number of hidden units if hidden_layer = True.
        self.hidden_units = 25

        # This parameter is called the step size, also known as the learning rate (lr).
        # See 18.6.1 in AIMA 3rd edition (page 719).
        # This is the value of α on Line 25 in Figure 18.24.
        self.lr = 1e-3

        # Line 6 in Figure 18.24 says "repeat".
        # This is the number of times we are going to repeat. This is often known as epochs.
        self.epochs = 400

        # We are going to store the data here.
        # Since you are only asked to implement training for the feed-forward neural network,
        # only self.x_train and self.y_train need to be used. You will need to use them to implement train().
        # The self.x_test and self.y_test is used by the unit tests. Do not change anything in it.
        self.x_train, self.y_train = None, None
        self.x_test, self.y_test = None, None

        # TODO: Make necessary changes here. For example, assigning the arguments "input_dim" and "hidden_layer" to
        # variables and so forth.

        # Assigning the input variables
        self.input_dim = input_dim
        self.hidden_layer = hidden_layer

        # Vector of errors
        self.delta = np.array([])

        # Lagrer verdiene til nettverket som skal opprettes etter train har blitt kjørt
        self.weights = []
        self.bias_weights = []
        self.bias_weight_output = 0

    def load_data(self, file_path: str = os.path.join(os.getcwd(), 'data_breast_cancer.p')) -> None:
        """
        Do not change anything in this method.

        Load data for training and testing the model.
        :param file_path: Path to the file 'data_breast_cancer.p' downloaded from Blackboard. If no arguments is given,
        the method assumes that the file is in the current working directory.

        The data have the following format.
                   (row, column)
        x: shape = (number of examples, number of features)
        y: shape = (number of examples)
        """
        with open(file_path, 'rb') as file:
            data = pickle.load(file)
            self.x_train, self.y_train = data['x_train'], data['y_train']
            self.x_test, self.y_test = data['x_test'], data['y_test']

    def train(self) -> None:
        """Run the backpropagation algorithm to train this neural network"""
        # TODO: Implement the back-propagation algorithm outlined in Figure 18.24 (page 734) in AIMA 3rd edition.
        # Only parts of the algorithm need to be implemented since we are only going for one hidden layer.

        # Line 6 in Figure 18.24 says "repeat".
        # We are going to repeat self.epochs times as written in the __init()__ method.

        # Line 27 in Figure 18.24 says "return network". Here you do not need to return anything as we are coding
        # the neural network as a class

        # Generates weights for the whole network. Weights are between -0.5 and 0.5
        # All weights from one input node are stored in a array inside the weights ndarray, all the weights from the
        # hidden units are stored in one common array, the last array in the weights ndarray
        weights1 = np.array(np.zeros((self.input_dim, self.hidden_units)))
        weights2 = np.zeros((1, self.hidden_units))

        for i in range(self.input_dim):
            for j in range(self.hidden_units):
                weights1[i][j] = np.random.rand()
                if weights1[i][j] >= 0.5:
                    weights1[i][j] -= 1
        for i in range(self.hidden_units):
            weights2[0][i] = np.random.rand()
            if weights2[0][i] >= 0.5:
                weights2[0][i] -= 1
        weights = np.vstack((weights1, weights2))

        # Lager en liste med bias weights for hidden layer og en enkel bias weight for outputen
        bias_weights = np.random.rand(25)
        for i in range(self.hidden_units):
            if bias_weights[i] >= 0.5:
                bias_weights[i] -= 1
        bias_weight_output = np.random.rand()
        if bias_weight_output >= 0.5:
            bias_weight_output -= 1

        counter = 0
        while counter < self.epochs:    # Gjentar algoritmen like mange ganger som det er epochs
            counter += 1
            for i in range(len(self.y_train)):

                features = self.x_train[i][:self.input_dim]     # Henter ut input verdiene for eksempel x_i

                if self.hidden_layer:   # Tilfellet med hidden layer

                    input_values = bias_weights + (np.transpose(weights[:-1]) * features).sum(axis=1)   # Verdiene som går inn i hidden layer sine noder

                    unit_values = self.g(input_values)     # outputverdiene fra nodene i hidden layer

                    output = self.g(bias_weight_output + np.sum(weights[-1] * unit_values))     # Output verdien i output noden

                    output_error = self.g_prime(bias_weight_output + np.sum(weights[-1] * unit_values)) * (self.y_train[i] - output)  # regner ut feil i outputnoden

                    hidden_errors = self.g_prime(input_values) * weights[-1] * output_error     # Feilen i nodene i hidden layer

                    for p in range(len(weights) - 1):
                        for j in range(len(weights[p])):
                            weights[p][j] += self.lr * features[p] * hidden_errors[j]   # Endrer vektene i forhold til feilen

                    weights[-1] += self.lr * unit_values * output_error # Endrer vektene som går til outputnoden basert på feilen i outputnoden

                    # Oppdaterer bias vektene til hidden layer
                    bias_weights += self.lr * hidden_errors

                else:   #Perceptron tilfellet

                    weights = np.transpose(weights[:-1])[0]     # Setter vektene til de første vektene i hver array

                    output = self.g(bias_weight_output + np.sum(weights * features))    # Output verdien til Outputnoden

                    output_error = self.g_prime(bias_weight_output + np.sum(weights * features)) * (self.y_train[i] - output)   # Feilen i Outputverdien

                    weights += self.lr * features * output_error    # Endrer vektene som går til outputnoden

                # oppdaterer bias vekten til output noden
                bias_weight_output = bias_weight_output + self.lr * output_error

        self.weights = weights
        self.bias_weights = bias_weights
        self.bias_weight_output = bias_weight_output

    def g(self, x):
        return 1 / (1 + np.exp(-x))

    def g_prime(self, x):
        return self.g(x)*(1 - self.g(x))

    def predict(self, x: np.ndarray) -> float:
        """
        Given an example x we want to predict its class probability.
        For example, for the breast cancer dataset we want to get the probability for cancer given the example x.
        :param x: A single example (vector) with shape = (number of features)
        :return: A float specifying probability which is bounded [0, 1].
        """
        # TODO: Implement the forward pass.

        if self.hidden_layer:
            hidden_unit_values = self.g(self.bias_weights + (np.transpose(self.weights[:-1]) * x).sum(axis=1))
            prediction = np.sum(hidden_unit_values * self.weights[-1])
        else:
            prediction = np.sum(x * self.weights)

        return self.g(prediction + self.bias_weight_output)


class TestAssignment5(unittest.TestCase):
    """
    Do not change anything in this test class.

    --- PLEASE READ ---
    Run the unit tests to test the correctness of your implementation.
    This unit test is provided for you to check whether this delivery adheres to the assignment instructions
    and whether the implementation is likely correct or not.
    If the unit tests fail, then the assignment is not correctly implemented.
    """

    def setUp(self) -> None:
        self.threshold = 0.8
        self.nn_class = NeuralNetwork
        self.n_features = 30

    def get_accuracy(self) -> float:
        """Calculate classification accuracy on the test dataset."""
        self.network.load_data()
        self.network.train()

        n = len(self.network.y_test)
        correct = 0
        for i in range(n):
            # Predict by running forward pass through the neural network
            pred = self.network.predict(self.network.x_test[i])
            # Sanity check of the prediction
            assert 0 <= pred <= 1, 'The prediction needs to be in [0, 1] range.'
            # Check if right class is predicted
            correct += self.network.y_test[i] == round(float(pred))
        return round(correct / n, 3)

    def test_perceptron(self) -> None:
        """Run this method to see if Part 1 is implemented correctly."""

        self.network = self.nn_class(self.n_features, False)
        accuracy = self.get_accuracy()
        self.assertTrue(accuracy > self.threshold,
                        'This implementation is most likely wrong since '
                        f'the accuracy ({accuracy}) is less than {self.threshold}.')

    def test_one_hidden(self) -> None:
        """Run this method to see if Part 2 is implemented correctly."""

        self.network = self.nn_class(self.n_features, True)
        accuracy = self.get_accuracy()
        self.assertTrue(accuracy > self.threshold,
                        'This implementation is most likely wrong since '
                        f'the accuracy ({accuracy}) is less than {self.threshold}.')


if __name__ == '__main__':
    unittest.main()
    # Kjøretiden skal være ca 200 sek. (3 min og 20 sek), men den kan av og til bruke lengre tid.
