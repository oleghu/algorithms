import pandas
import math
import numpy as np
from graphviz import Digraph

class Tree: #Definerer en egen tre klasse som har en dictonary med undertrær
    def __init__(self, root):
        self.root = root
        self.children = {}
        self.childrenlist = []

    def addbranch(self, value, subtree):
        self.children[value] = subtree
        self.childrenlist.append(value)


def tree_builder():
    df = pandas.read_csv('titanic/test.csv')
    df.pop('Embarked')
    continous_columns = []
    for i in range(len(df.columns)):
        if len(df[df.columns[i]].value_counts()) >= 10: #definerer alle attributter med mer en 10 mulige verdier som kontinuerlige
            continous_columns.append(i)
    for i in range(len(continous_columns)):
        df.pop(df.columns[continous_columns[i]])
        continous_columns = [x - 1 for x in continous_columns]
    print(df)
    treet = decision_tree_learning(df, df.columns, df)
    print(accuracy(treet, df))  #printer nøyaktigheten

    u = Digraph('unix', filename='unix.gv',
                node_attr={'color': 'lightblue1', 'style': 'filled'})
    u.attr(size='6,6')
    draw(treet, df, u, 0, 0)
    u.view()    #tegner treet

def decision_tree_learning(examples, attributes, parent_examples):
    if len(examples.values) == 0: #Plurality-value
        return plurality_value(parent_examples)

    if len(examples.value_counts('Survived')) == 1: #if all examples have the same classification
        return examples.value_counts('Survived').index[0]

    if len(attributes) == 1:
        return plurality_value(examples)
    attribute = []
    for i in range(1, len(attributes)):
        attribute.append(importance(attributes[i], examples))
    a = attributes[np.argmax(attribute)+1]  #selects the attribute with the highest information gain
    tree = Tree(a)
    attribute_distribution = examples[a].value_counts()
    new_attributes = attributes.drop(a)

    for i in range(len(attribute_distribution)):    #itererer gjennom de mulige verdiene attributten kan ha
        exs = examples.loc[examples[a] == attribute_distribution.index[i]]
        subtree = decision_tree_learning(exs, new_attributes, examples) #rekursivt kall og genererer barne nodene til attributtet
        tree.addbranch(attribute_distribution.index[i], subtree)    #legger til treet som en branch

    return tree




def plurality_value(parent_examples):
    if parent_examples['Survived'].value_counts()[0] >= parent_examples['Survived'].value_counts()[1]:
        return 0
    else:
        return 1

def importance(a, examples):
    survivor_distribution = examples['Survived'].value_counts()

    tot = entropy(survivor_distribution[1]/(survivor_distribution[0]+survivor_distribution[1]))

    attribute_distribution = examples[a].value_counts()

    for i in range(0, len(attribute_distribution)):
        s_d_a = examples.loc[examples[a] == attribute_distribution.index[i]] #survivor distrinution with certain attribute value

        if len(s_d_a['Survived'].value_counts()) == 1:
            continue

        tot -= (s_d_a['Survived'].value_counts()[0] + s_d_a['Survived'].value_counts()[1]) / \
               (examples['Survived'].value_counts()[0] + examples['Survived'].value_counts()[1]) * \
               entropy(s_d_a['Survived'].value_counts()[1]/(s_d_a['Survived'].value_counts()[0] + s_d_a['Survived'].value_counts()[1]))
    return tot


def entropy(probability):
    return -(probability * math.log(probability, 2) + (1 - probability) * math.log((1 - probability), 2))

def accuracy(tree, df):
    if tree == 1 or tree == 0:  #Hvis vi har nådd en endenode
        count = df['Survived'].value_counts()
        if tree == 1:   #hvis endenoden er 1
            if len(count) == 1 and count.index[0] == 1:
                return 1
            elif len(count) == 1 and count.index[0] == 0:
                return 0
            return count[1]/(count[0]+count[1])
        else:   #hvis endenoden er 0
            if len(count) == 1 and count.index[0] == 0:
                return 1
            elif len(count) == 1 and count.index[0] == 1:
                return 0
            return count[0]/(count[0]+count[1])
    #de forskjellige scenarione beskrevet i de øvrige if setningene ser på froskjellige varianter av endenoder og ser om det stemmer med dataen
    attributes = []
    tots = []
    for key in tree.children:
        attributes.append(key)
    for i in range(len(tree.childrenlist)):
        b = tree.children.get(tree.childrenlist[i]) #henter treet der en viss verdi har blitt valgt for attributtet
        c = df.loc[df[tree.root] == attributes[i]]  #lager et datasett der attributtet har en bestemt verdi
        tots.append(accuracy(b, c)) #gjør et rekursivt kall for å gå ett hakk lengre ned i treet

    return sum(tots)/len(tots)

def draw(tree, df, graph, counter, parentnode):
    counter += 1
    if tree == 0 or tree == 1:
        return tree

    attributes = []
    for key in tree.children:
        attributes.append(key)

    for i in range(len(tree.childrenlist)):
        b = tree.children.get(tree.childrenlist[i])
        c = df.loc[df[tree.root] == attributes[i]]

        if counter == 1 and (b == 1 or b == 0):
            graph.node(str(counter) + str(i) + str(i) + str(parentnode), str(b))
            graph.edge(str(counter), str(counter) + str(i) + str(i) + str(parentnode), label=str(attributes[i]))
            continue
        elif b == 1 or b == 0:
            graph.node(str(counter) + str(i) + str(i) + str(parentnode), str(b))
            graph.edge(parentnode, str(counter) + str(i) + str(i) + str(parentnode), label=str(attributes[i]))
            continue
        elif counter == 1:
            graph.node(str(counter), tree.root)
            graph.node(str(counter) + str(i) + str(parentnode), b.root)
            graph.edge(str(counter), str(counter) + str(i) + str(parentnode), label=str(attributes[i]))
        else:
            graph.node(str(counter) + str(i) + str(parentnode), b.root)
            graph.edge(parentnode, str(counter) + str(i) + str(parentnode), label=str(attributes[i]))

        draw(b, c, graph, counter, str(counter) + str(i) + str(parentnode)) #rekursivt kall for å traversere ned treet

#Alle de forskjellige nodene for unike id'er slik at noder med samme label kan vises flere ganger

if __name__ == '__main__':
    tree_builder()
