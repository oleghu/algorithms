import pandas
import math
import numpy as np
from graphviz import Digraph


class Tree:
    def __init__(self, root):
        self.root = root
        self.children = {}
        self.childrenlist = []

    def addbranch(self, value, subtree):
        self.children[value] = subtree
        self.childrenlist.append(value)

def draw(tree, df, graph, counter, parentnode):
    counter += 1
    if tree == 0 or tree == 1:
        return

    attributes = []
    for key in tree.children:
        attributes.append(key)

    for i in range(len(tree.childrenlist)):
        if counter == 1:
            graph.node(str(counter), tree.root)
            graph.node(str(counter) + str(i), str(attributes[i]))
            graph.edge(str(counter), str(counter) + str(attributes[i]))
        else:
            graph.node(str(counter) + str(i), str(attributes[i]))
            graph.edge(parentnode, str(counter) + str(attributes[i]))
        b = tree.children.get(tree.childrenlist[i])
        c = df.loc[df[tree.root] == attributes[i]]
        d = tree.children[tree.childrenlist[i]]
        if d == 0 or d == 1:
            graph.node(str(counter) + str(i) + str(i), str(d))
            graph.edge(str(counter) + str(i), str(counter) + str(i) + str(i))
            continue
        e = d.root
        graph.node(str(counter)+str(i)+str(i), tree.children[tree.childrenlist[i]].root)
        graph.edge(str(counter)+str(i), str(counter)+str(i)+str(i))
        draw(b, c, graph, counter, str(counter)+str(i))

if __name__ == '__main__':
    treet = Tree('sol')
    treet.children = {'1': [0, 'mat'],
                      '0': [0, 0]
                      }
    u = Digraph('unix', filename='unix.gv',
                node_attr={'color': 'lightblue2', 'style': 'filled'})
    u.attr(size='6,6')
    draw(treet, df, u, 0, 0)
    u.view()
