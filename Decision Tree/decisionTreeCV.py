import pandas
import math
import numpy as np
from graphviz import Digraph

class Tree: #Definerer en egen tre klasse som har en dictonary med undertrær
    def __init__(self, root):
        self.root = root
        self.children = {}
        self.childrenlist = []

    def addbranch(self, value, subtree):
        self.children[value] = subtree
        self.childrenlist.append(value)


def tree_builder():
    df = pandas.read_csv('titanic/test.csv')
    df.pop('Embarked')
    df.pop('Name')
    df.pop('Ticket')
    df.pop('Cabin')
    df.pop('Age')
    continous = ['Fare'] #liste med attributtene jeg anser som kontinuerlige
    print(df)
    treet = decision_tree_learning(df, df.columns, df, continous)
    print(accuracy(treet, df, continous)) #printer nøyaktigheten

    u = Digraph('unix', filename='unix.gv',
                node_attr={'color': 'lightblue1', 'style': 'filled'})
    u.attr(size='6,6')
    draw(treet, df, u, 0, 0)
    u.view()    #tegner treet

def decision_tree_learning(examples, attributes, parent_examples, continous):
    if len(examples.values) == 0: #Plurality-value
        return plurality_value(parent_examples)

    if len(examples.value_counts('Survived')) == 1: #if all examples have the same classification
        return examples.value_counts('Survived').index[0]

    if len(attributes) == 1:
        return plurality_value(examples)

    attribute = []

    for i in range(1, len(attributes)):
        if attributes[i] in continous: #i denne if statmenten håndterer jeg kontinuerlige variabler
            bestsplit = []
            previous = 0

            for j in range(len(examples[attributes[i]])):
                kopi = examples.copy()
                kopi = kopi.sort_values(by=attributes[i])   #sorterer etter den gitte kontinuerlige attributten

                if kopi[attributes[i]][kopi.index[j]] != previous:  #hvis verdien for attributten er ulik den forrige verdien

                    test = kopi[attributes[i]].apply(lambda x: '<='+str(kopi[attributes[i]][kopi.index[j]])
                    if x <= kopi[attributes[i]][kopi.index[j]] else '>'+str(kopi[attributes[i]][kopi.index[j]]))
                    # splitter og setter alle verdier under eller lik splittverdien til <=splittverdien, og alle verdier over settes til >splittverdiem
                    previous = kopi[attributes[i]][kopi.index[j]]
                    kopi[attributes[i]] = test
                    importance1 = importance(attributes[i], kopi)
                    bestsplit.append(importance1)

                    if bestsplit[np.argmax(bestsplit)] == importance1:
                        bestsplit_info = kopi   #husker hvordan dataframen ser ut for den beste splitten"
            attribute.append(bestsplit[np.argmax(bestsplit)])   #velger den beste splitten og legger den til i attributter listen som inneholder
                                                                #information gain for alle attributtene
        else:
            attribute.append(importance(attributes[i], examples))
    a = attributes[np.argmax(attribute)+1] #velger attributtene med høyest information gain
    tree = Tree(a)

    if a in continous: #hvis attributten er en kontinuerlig variabel
        attribute_distribution = bestsplit_info[a].value_counts()
        new_attributes = attributes.drop(a)
        for i in range(len(attribute_distribution)): #itererer over de to verdiene attrbiutten nå kan ha etter splitten
            exs = bestsplit_info.loc[bestsplit_info[a] == attribute_distribution.index[i]]
            subtree = decision_tree_learning(exs, new_attributes, bestsplit_info, continous)    #rekursivt kall og genererer barne nodene til attributtet
            tree.addbranch(attribute_distribution.index[i], subtree)    #legger til treet som en branch
    else:
        attribute_distribution = examples[a].value_counts()
        new_attributes = attributes.drop(a)
        for i in range(len(attribute_distribution)):
            exs = examples.loc[examples[a] == attribute_distribution.index[i]]
            subtree = decision_tree_learning(exs, new_attributes, examples, continous)  #rekursivt kall og genererer barne nodene til attributtet
            tree.addbranch(attribute_distribution.index[i], subtree)    #legger til treet som en branch

    return tree


def plurality_value(parent_examples):
    if parent_examples['Survived'].value_counts()[0] >= parent_examples['Survived'].value_counts()[1]:
        return 0
    else:
        return 1

def importance(a, examples):

    survivor_distribution = examples['Survived'].value_counts()

    tot = entropy(survivor_distribution[1]/(survivor_distribution[0]+survivor_distribution[1]))

    attribute_distribution = examples[a].value_counts()

    for i in range(0, len(attribute_distribution)):
        s_d_a = examples.loc[examples[a] == attribute_distribution.index[i]] #survivor distrinution with certain attribute value

        if len(s_d_a['Survived'].value_counts()) == 1:
            continue

        tot -= (s_d_a['Survived'].value_counts()[0] + s_d_a['Survived'].value_counts()[1]) / \
               (examples['Survived'].value_counts()[0] + examples['Survived'].value_counts()[1]) * \
               entropy(s_d_a['Survived'].value_counts()[1]/(s_d_a['Survived'].value_counts()[0] + s_d_a['Survived'].value_counts()[1]))
    return tot


def entropy(probability):
    return -(probability * math.log(probability, 2) + (1 - probability) * math.log((1 - probability), 2))



def accuracy(tree, df, continous):
    if tree == 1 or tree == 0:
        count = df['Survived'].value_counts()
        if tree == 1:
            if len(count) == 1 and count.index[0] == 1:
                return 1
            elif len(count) == 1 and count.index[0] == 0:
                return 0
            return count[1]/(count[0]+count[1])
        else:
            if len(count) == 1 and count.index[0] == 0:
                return 1
            elif len(count) == 1 and count.index[0] == 1:
                return 0
            return count[0]/(count[0]+count[1])
    #de forskjellige scenarione beskrevet i de øvrige if setningene ser på froskjellige varianter av endenoder og ser om det stemmer med dataen

    attributes = []
    tots = []
    for key in tree.children:
        attributes.append(key)
    if tree.root in continous:
        for i in range(len(tree.childrenlist)): #ettersom jeg ikke har mulighet til å hente bestsplit_info her, så lager jeg en kopi av hvordan den ville sett ut
                                                #ut ifra informasjonen jeg har av splitt atributten
            b = tree.children.get(tree.childrenlist[i]) #henter treet der en viss verdi har blitt valgt for attributtet
            if attributes[i][:2] == '<=':
                test = float(attributes[i][2:])
                test2 = df[tree.root].apply(lambda x: attributes[i] if x <= test else '>'+str(attributes[i][2:]))
            else:
                test = float(attributes[i][1:])
                test2 = df[tree.root].apply(lambda x: '<=' + str(attributes[i][1:]) if x <= test else attributes[i])

            c = df.copy()
            c[tree.root] = test2    #lager et datasett der attributtet har en bestemt verdi
            tots.append(accuracy(b, c, continous)) #gjør et rekursivt kall for å gå ett hakk lengre ned i treet
    else:
        for i in range(len(tree.childrenlist)):
            b = tree.children.get(tree.childrenlist[i]) #henter treet der en viss verdi har blitt valgt for attributtet
            c = df.loc[df[tree.root] == attributes[i]]   #lager et datasett der attributtet har en bestemt verdi
            tots.append(accuracy(b, c, continous))  #gjør et rekursivt kall for å gå ett hakk lengre ned i treet

    return sum(tots)/len(tots)

def draw(tree, df, graph, counter, parentnode):
    counter += 1
    if tree == 0 or tree == 1:
        return tree

    attributes = []
    for key in tree.children:
        attributes.append(key)

    for i in range(len(tree.childrenlist)):
        b = tree.children.get(tree.childrenlist[i])
        c = df.loc[df[tree.root] == attributes[i]]

        if counter == 1 and (b == 1 or b == 0):
            graph.node(str(counter) + str(i) + str(i) + str(parentnode), str(b))
            graph.edge(str(counter), str(counter) + str(i) + str(i) + str(parentnode), label=str(attributes[i]))
            continue
        elif b == 1 or b == 0:
            graph.node(str(counter) + str(i) + str(i) + str(parentnode), str(b))
            graph.edge(parentnode, str(counter) + str(i) + str(i) + str(parentnode), label=str(attributes[i]))
            continue
        elif counter == 1:
            graph.node(str(counter), tree.root)
            graph.node(str(counter) + str(i) + str(parentnode), b.root)
            graph.edge(str(counter), str(counter) + str(i) + str(parentnode), label=str(attributes[i]))
        else:
            graph.node(str(counter) + str(i) + str(parentnode), b.root)
            graph.edge(parentnode, str(counter) + str(i) + str(parentnode), label=str(attributes[i]))

        draw(b, c, graph, counter, str(counter) + str(i) + str(parentnode)) #rekursivt kall for å traversere ned treet

#Alle de forskjellige nodene for unike id'er slik at noder med samme label kan vises flere ganger


if __name__ == '__main__':
    tree_builder()
